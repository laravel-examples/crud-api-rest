@extends('products.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="float-left">
                <h2>Laravel 7 - Product CRUD by REST API</h2>
            </div>
            <div class="float-right">
                <button class="btn btn-success" type="button" id="createNewProduct">
                    Create New Product
                </button>
            </div>
        </div>
    </div>

    <table class="table table-bordered" id="productsDatatable" width="100%">
        <thead>
            <tr>
                <th>N°</th>
                <th>Name</th>
                <th>Details</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <!-- The Modal -->
    <div class="modal fade" id="productModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title" id="productModalTitle"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <form action="" method="post" id="productForm">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="typeAction" id="typeAction">
                    <div class="form-group">
                        <label for="name"><strong>Name</strong></label>
                        <input class="form-control" type="text" name="name" id="name" placeholder="Enter product name">
                    </div>
                    <div class="form-group">
                        <label for="detail"><strong>Details</strong></label>
                        <textarea class="form-control" name="detail" id="detail" placeholder="Enter product details"></textarea>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <input class="btn btn-primary" type="submit" value="Submit">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var products_datatable = $('#productsDatatable').DataTable({
            ajax: {
                url: "http://localhost:8000/api/products",
                type: "GET"
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'detail', name: 'detail'},
                {data: 'null', render: function(data, type, row) {
                    var btn = '<button class="btn btn-primary btn-sm editProduct" type="button" data-id="' + row.id + '">Edit</button>';
                    btn = btn + ' <button class="btn btn-danger btn-sm deleteProduct" type="button" data-id="' + row.id + '">Delete</button>';

                    return btn;
                }},
            ]
        });

        $('#createNewProduct').click(function(event) {
            $('#productModalTitle').text('Create Product');
            $('#typeAction').val('create');
            $('#productModal').modal('show');
        });

        $('#productsDatatable tbody').on('click', '.editProduct', function(event) {
            var product_id = $(this).data('id');

            $.ajax({
                url: "http://localhost:8000/api/products/" + product_id,
                type: "GET",
                data: $('#productForm').serialize(),
                datatype: 'json',
                success: function(response) {
                    $('#productForm').trigger('reset');
                    $('#productModalTitle').text('Edit Product');
                    $('#typeAction').val('update');
                    $('#id').val(response.data.id);
                    $('#name').val(response.data.name);
                    $('#detail').val(response.data.detail);
                    $('#productModal').modal('show');
                },
                error: function(response) {
                    alert('Error: ' + JSON.stringify(response));
                    products_datatable.ajax.reload();
                }
            });
        });

        $('#productForm').submit(function(event) {
            event.preventDefault();
            var type_action = $('#typeAction').val();
            if (type_action == 'create') {
                var url = "http://localhost:8000/api/products";
                var type = "POST";
            } else if (type_action == 'update') {
                var product_id = $('#id').val();
                var url = "http://localhost:8000/api/products/" + product_id;
                var type = "PUT";
            }

            $.ajax({
                url: url,
                type: type,
                data: $('#productForm').serialize(),
                datatype: 'json',
                success: function(response) {
                    $('#productModal').modal('hide');
                    $('#productForm').trigger('reset');
                    $('#productModalTitle').text("");
                    $('#typeAction').val("");
                    $('#id').val("");
                    products_datatable.ajax.reload();
                },
                error: function(response) {
                    alert('Error: ' + JSON.stringify(response));
                    products_datatable.ajax.reload();
                }
            });
        });

        $('#productsDatatable tbody').on('click', '.deleteProduct', function(event) {
            var product_id = $(this).data('id');

            var delete_product = confirm('Are you sure want to delete?');

            if (delete_product == true) {
                $.ajax({
                    url: "http://localhost:8000/api/products/" + product_id,
                    type: "DELETE",
                    datatype: 'json',
                    success: function(response) {
                        products_datatable.ajax.reload();
                    },
                    error: function(response) {
                        alert('Error: ' + JSON.stringify(response));
                        products_datatable.ajax.reload();
                    }
                });
            } else {
                products_datatable.ajax.reload();
            }
        });
    });
</script>
@endsection
